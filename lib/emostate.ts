import { useRef, useState } from 'react'
import { RecoilState, useRecoilState } from 'recoil'

export function useFunnyState<S>(initialState: S | (() => S)) {
    const [_value, setValue] = useState<S>(initialState)
    const counterRef = useRef<any>(initialState)

    return {
        get value(): S {
            return counterRef.current
        },
        set value(val) {
            counterRef.current = val
            setValue(val)
        }
    }
}

export function useHappyState<S>(recoilState: RecoilState<S>) {
    const [_value, setValue] = useRecoilState<S>(recoilState)

    return {
        get value(): S {
            const obj = JSON.parse(localStorage.getItem(recoilState.key) ?? 'null')
            if (Object.prototype.toString.call(obj) !== Object.prototype.toString.call(_value)) {
                return _value
            }
            return obj
        },
        set value(val) {
            localStorage.setItem(recoilState.key, JSON.stringify(val))
            setValue(val!)
        }
    }
}
